import math as mt

def main():
    while True:
        str = input("Выберите действие:\n1)Калькулятор\n2)Подсчёт в строке\n3)Матрица\n\n0)Выход\n")
        if (str == '1') | (str == '2') | (str == '3'):
            if str == '1':
                calculator()
            elif str == '2':
                string()
            elif str == '3':
                matrix()
        elif str == '0':
            break
        else:
            print("Ошибка!")

def calculator():
    while True:
        try:
            k = int(input("Выберите действие:\n1)сложение\n2)вычитание\n3)умножение\n4)деление\n5)возведение в степень\n6)целочисленное деление\n7)остаток от деления\n8)смена знака\n9)факториал\n10)логарифм по основанию 10\n11)косинус\n12)синус\n13)площадь параллелограмма\n\n0)вернуться в меню\n"))
            if (k >= 1) & (k <= 7):
                while True:
                    try:
                        k1 = int(input("Введите первое число: "))
                        k2 = int(input("Введите второе число: "))
                        if k == 1:
                            print("Результат: ",k1+k2)
                        elif k == 2:
                            print("Результат: ",k1-k2)
                        elif k == 3:
                            print("Результат: ",k1*k2)
                        elif k == 4:
                            print("Результат: ",k1/k2)
                        elif k == 5:
                            print("Результат: ",k1**k2)
                        elif k == 6:
                            print("Результат: ",k1//k2)
                        elif k == 7:
                            print("Результат: ",k1%k2)
                        break
                    except Exception:
                        print("Ошибка!")
            elif (k >= 8) & (k <= 12):
                while True:
                    try:
                        k1 = int(input("Введите число: "))
                        if k == 8:
                            print("Результат: ",-k1)
                        elif k == 9:
                            print("Результат: ",mt.factorial(k1)) 
                        elif k == 10:
                            print("Результат: ",mt.log10(k1)) 
                        elif k == 11:
                            print("Результат: ",mt.cos(k1)) 
                        elif k == 12:
                            print("Результат: ",mt.sin(k1)) 
                        break
                    except Exception:
                        print("Ошибка!")
            elif k == 13:
                while True:
                    try:
                        lam = lambda u, i: u*i
                        a = int(input("Введите основание: "))
                        h = int(input("Введите высоту: "))
                        print("Результат: ",lam(a,h))
                        break
                    except Exception:
                        print("Ошибка!")
            elif k == 0:
                break
            else:
                print("Ошибка!")
        except Exception:
            print("Ошибка!")

def string():
    while True:
        s = input("Введите строку: ")
        print("Количество символов: ",len(s))
        print("Количество пробелов: ",s.count(' '))
        print("Количество запятых: ",s.count(','))
        k = input("Нажмите на любой символ, если хотите продолжить\nНажмите на 0, если хотите вернуться в меню\n")
        if k == '0':
            break

def matrix():
    while True:
        try:
            columns = input("Введите количество столбцов: ")
            if (columns == ''):
                mas = []
                start = 0
                for column in range(10):
                    mas.append([])
                    for line in range (10):
                        mas[column].append(start)
                        print(mas[column][line], end = ' ') 
                        start += 1
                    print()
            else:
                columns = int(columns)
                lines = int(input("Введите количество строк: ")) 
                start = int(input("Введите число, с которого начнётся матрица: ")) 
                step = int(input("Введите шаг: ")) 

                mas=[]
                for column in range(columns):
                    mas.append([])
                    for line in range (lines):
                        mas[column].append(start)
                        print(mas[column][line], end = ' ') 
                        start += step
                    print()
                k = input("Нажмите на любой символ, если хотите продолжить\nНажмите на 0, если хотите вернуться в меню\n")
                if k == '0':
                    break
        except Exception:
            print("Ошибка!")

main()