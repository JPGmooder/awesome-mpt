def matrix():
    while True:
        try:
            columns = input("Введите количество столбцов: ")
            if (columns == ''):
                mas = []
                start = 0
                for column in range(10):
                    mas.append([])
                    for line in range (10):
                        mas[column].append(start)
                        print(mas[column][line], end = ' ') 
                        start += 1
                    print()
            else:
                columns = int(columns)
                lines = int(input("Введите количество строк: ")) 
                start = int(input("Введите число, с которого начнётся матрица: ")) 
                step = int(input("Введите шаг: ")) 

                mas=[]
                for column in range(columns):
                    mas.append([])
                    for line in range (lines):
                        mas[column].append(start)
                        print(mas[column][line], end = ' ') 
                        start += step
                    print()
                k = input("Нажмите на любой символ, если хотите продолжить\nНажмите на 0, если хотите вернуться в меню\n")
                if k == '0':
                    break
        except Exception:
            print("Ошибка!")

